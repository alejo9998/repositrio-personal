package com.example.prototipo3

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QueryDocumentSnapshot
import kotlinx.android.synthetic.main.activity_log_in.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import java.lang.Exception


class LogIn : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    lateinit var pref:SharedPreferences
    val db = FirebaseFirestore.getInstance()
    lateinit var dbRef: DocumentReference
    var debo:String=""
    var deben:String=""
    var metd=false
    private lateinit var dbRef2:CollectionReference
    lateinit var dbref: DocumentReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        auth = FirebaseAuth.getInstance()
        dbRef2=db.collection("usuarios")

        setContentView(R.layout.activity_log_in)
        pref= PreferenceManager.getDefaultSharedPreferences(this)
        val correo: EditText = findViewById(R.id.textUsuario)
        val contr: EditText = findViewById(R.id.textContra)
        val name : EditText = findViewById(R.id.nombre)

        val botCrear: TextView = findViewById(R.id.crearCuenta)
        nombre.visibility=View.GONE
        val botIngresar: Button = findViewById(R.id.btnIngresar)

        checkCon()

        botIngresar.setOnClickListener()
        {
            if(metd)
            {
                crear()
            }
            else {
                iniciar()
            }
        }

        botCrear.setOnClickListener()
        {
            correo.text.clear()
            contr.text.clear()
            name.text.clear()
            metd=!metd
            if(metd) {
                nombre.visibility=View.VISIBLE
                botIngresar.text = "Crear Cuenta"
                botCrear.text = "Ya tengo cuenta"
                Log.i("click",botCrear.isClickable.toString())
            }
            else
            {
                nombre.visibility=View.GONE
                botIngresar.text = "Ingresa"
                botCrear.text = "Soy nuevo, crear una cuenta"

            }

        }

    }
    fun crear(){
        val correo: EditText = findViewById(R.id.textUsuario)
        val contr: EditText = findViewById(R.id.textContra)
        val name : EditText = findViewById(R.id.nombre)



        if((!(name.text.toString()).equals(""))&& !((correo.text.toString()).equals(""))&& !((contr.text.toString()).equals("")) )
        {
            if(checkCon())
            {

                auth.createUserWithEmailAndPassword(correo.text.toString(), contr.text.toString())
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {


                            Toast.makeText(
                                this,
                                "Se creo correctamente el usuario",
                                Toast.LENGTH_LONG
                            ).show()
                            val userp = hashMapOf(
                                "nombre" to name.text.toString(),
                                "email" to correo.text.toString(),
                                "ID" to task.result!!.user!!.uid


                            )
                            dbRef2.add(userp)
                            var i = Intent(this, MainActivity::class.java)
                            startActivity(i)
                            finish()
                        } else {
                            Toast.makeText(this, "Error al crear usuario", Toast.LENGTH_LONG).show()

                        }

                    }
            }
            else
            {
                progressBar3.visibility=View.GONE
                showAlertInternet()

            }
        }
        else
        {
            Log.i("else","cae en el if")
        }
    }

    fun iniciar()
    {
        val botIngresar: Button = findViewById(R.id.btnIngresar)
        progressBar3.visibility=View.VISIBLE
        if(checkCon()){
            var correo: EditText = findViewById(R.id.textUsuario)
            var contr: EditText = findViewById(R.id.textContra)
            if (textUsuario.text.equals("") || contr.text.toString().equals("")) {
                progressBar3.visibility=View.GONE
                Toast.makeText(
                    this,
                    "Completen los campos de correo y contraseña",
                    Toast.LENGTH_LONG
                ).show()
            } else {

                botIngresar.isEnabled = false

                auth.signInWithEmailAndPassword(correo.text.toString(), contr.text.toString())
                    .addOnCompleteListener(this)
                    { task ->
                        if (task.isSuccessful) {

                            save(
                                correo.text.toString(),
                                contr.text.toString(),
                                task.result!!.user!!.uid
                            )

                            db.collection("usuarios").get()
                                .addOnSuccessListener { documents ->

                                    for (document in documents) {

                                        if (
                                            ((document.data.get("ID"))!!.equals(task.result!!.user!!.uid))
                                        ) {
                                            dbRef = document.reference
                                        }
                                    }

                                    db.collection("prestamos").get()
                                        .addOnSuccessListener { documents ->
                                            for (document in documents) {

                                                if (
                                                    ((document.data.get("recibe").toString())!!.equals(
                                                        dbRef.toString()
                                                    ))
                                                ) {
                                                    try{
                                                    dbref =
                                                        document.get("presta") as DocumentReference

                                                        dbref.get()
                                                            .addOnSuccessListener { correo ->

                                                                debo =
                                                                    debo + correo.data!!.get("nombre") + "-" + document.get(
                                                                        "cantidad"
                                                                    )
                                                                        .toString() + "-" + document.get(
                                                                        "medida"
                                                                    )
                                                                        .toString() + "-" + document.get(
                                                                        "producto"
                                                                    ).toString() + ","
                                                                pref.edit().putString("debo", debo)
                                                                    .apply()

                                                            }
                                                    }
                                                    catch (e: Exception)
                                                    {
                                                        pref.edit().putString("debo", debo)
                                                            .apply()
                                                    }
                                                }

                                            }
                                        }

                                }
                                .addOnFailureListener { exception ->
                                    Log.i("error", "Error getting documents: ", exception)

                                }
                            db.collection("usuarios").get()
                                .addOnSuccessListener { documents ->

                                    for (document in documents) {

                                        if (
                                            ((document.data.get("ID"))!!.equals(task.result!!.user!!.uid))
                                        ) {
                                            dbRef=document.reference
                                        }
                                    }

                                    db.collection("prestamos").get()
                                        .addOnSuccessListener { documents ->
                                            for (document in documents) {

                                                if (
                                                    ((document.data.get("presta").toString())!!.equals(dbRef.toString()))
                                                )
                                                {
                                                    try{

                                                    dbref =
                                                        document.get("recibe") as DocumentReference


                                                    dbref.get().addOnSuccessListener { correo ->


                                                        deben =
                                                            deben + correo.data!!.get("nombre")
                                                                .toString() + "-" + document.get(
                                                                "cantidad"
                                                            )
                                                                .toString() + "-" + document.get("medida")
                                                                .toString() + "-" + document.get(
                                                                "producto"
                                                            ).toString() + ","
                                                        pref.edit().putString("deben", deben)
                                                            .apply()
                                                    }
                                                }
                                                    catch(e:Exception)
                                                    {
                                                        pref.edit().putString("deben", deben)
                                                            .apply()
                                                    }
                                                }
                                            }


                                        }
                                        .addOnCompleteListener{
                                            var i = Intent(this, MainActivity::class.java)
                                            startActivity(i)
                                            finish()}


                                }



                        } else {

                            showAlert()
                            botIngresar.isEnabled = true
                            progressBar3.visibility=View.GONE
                        }
                        var el=pref.getString("debo","")

                    }

            }
        }
        else
        {
            botIngresar.isEnabled = true
            showAlertInternet()
            progressBar3.visibility=View.GONE
        }
    }

    fun checkCon():Boolean{
        val cm=getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val ac=cm.activeNetworkInfo
        val isC= ac!=null && ac.isConnectedOrConnecting
        return isC
    }



    fun save(usu:String,cont:String,id:String)
    {

        var edit=pref.edit()
        edit.putString("usuario",usu).apply()
        edit.putString("contr",cont).apply()
        edit.putString("id",id).apply()
        Log.i("id",id)

    }
    fun showAlert()
    {
        progressBar3.visibility=View.GONE
        Toast.makeText(this,"Usuario o contraseña incorrecta",Toast.LENGTH_LONG).show()

    }
    fun showAlertInternet()
    {

        Toast.makeText(this,"No hay conexion a internet en este momento",Toast.LENGTH_LONG).show()
    }
}
