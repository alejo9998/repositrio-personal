package com.example.prototipo3

import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.BluetoothSocket
import android.preference.PreferenceManager
import android.util.Log
import java.util.*
import java.util.prefs.PreferenceChangeEvent

class BluetoothServer(private val activity: Activity, private val socket: BluetoothSocket): Thread() {
    private val inputStream = this.socket.inputStream
    private val outputStream = this.socket.outputStream
    private val id:String?=PreferenceManager.getDefaultSharedPreferences(activity).getString("id","No hay id")

    override fun run() {

    }

    fun dato(ped:String)
    {
        var pref= PreferenceManager.getDefaultSharedPreferences(activity)
        var debo =  pref.getString("deben","")
        if(debo.equals(""))
        {
            var x=debo.plus(ped)
            var ed=pref.edit()
            ed.putString("deben",x).apply()
        }
        else
        {
            var x=debo.plus(",").plus(ped)
            var ed=pref.edit()
            ed.putString("deben",x).apply()
        }
    }

}