package com.example.prototipo3

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.provider.CallLog.Calls.NEW
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import org.jetbrains.anko.find
import java.lang.StringBuilder


class SolicitudExitosa : AppCompatActivity() {

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    val db = FirebaseFirestore.getInstance()
    val auth = FirebaseAuth.getInstance()
    lateinit var pref: SharedPreferences
    lateinit var dbRef: DocumentReference
    lateinit var dbPres: DocumentReference
    lateinit var pred: String
    lateinit var preN:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pref = PreferenceManager.getDefaultSharedPreferences(this)
        setContentView(R.layout.activity_solicitud_exitosa)
        var correo: String? = pref.getString("usuario", "No hay")
        var contr: String? = pref.getString("contr", "No hay")
        var us = pref.getString("nom", "no hay")
        var id = pref.getString("id", "No hay")
        var idp = pref.getString("presta", "No hay")
        var pred = pref.getString("momen", "No hay")


        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        if(checkCon())
        {
        auth.signInWithEmailAndPassword(correo.toString(), contr.toString())
            .addOnCompleteListener(this)
            { task ->
                if (task.isSuccessful) {
                    db.collection("usuarios").get()
                        .addOnSuccessListener { documents ->
                            Log.i("antes del for", "antes")
                            for (document in documents) {

                                if (
                                    ((document.data.get("ID"))!!.equals(id))
                                ) {
                                    dbRef = document.reference
                                } else if (
                                    ((document.data.get("ID"))!!.equals(idp))
                                ) {
                                    preN=document.data.get("nombre").toString()
                                    dbPres = document.reference
                                }
                            }
                            var pedid = pred!!.split("-")
                            Log.i("pedido", pred)
                            val pedido = hashMapOf(
                                "recibe" to dbRef,
                                "producto" to pedid[3],
                                "presta" to dbPres,
                                "medida" to pedid[2],
                                "cantidad" to pedid[1]


                            )

                            db.collection("prestamos").add(pedido)
                            var el =pred.split("-")
                            var debo = pref.getString("debo", "")
                            if (debo.equals("No le debes a nadie")) {
                                var x = preN.plus("-").plus(el[1]).plus("-").plus(el[2]).plus("-").plus(el[3])
                                var ed = pref.edit()
                                ed.putString("debo", x).apply()
                            } else {
                                var x = preN.plus(el[1]).plus("-").plus(el[2]).plus("-").plus(el[3]).plus(",")
                                var ed = pref.edit()
                                ed.putString("debo", x).apply()
                            }
                            val bundle = Bundle()
                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "finalizado")
                            firebaseAnalytics.logEvent("Pedidosfinalizados", bundle)

                        }
                        .addOnFailureListener { exception ->
                            Log.i("error", "Error getting documents: ", exception)


                        }



                    finish()
                }
            }
    }
        else
        {
            showAlert()
            var debo = pref.getString("debo", "")
            if (debo.equals("No le debes a nadie")) {
                var x = debo.plus(us).plus("-").plus(pred)
                var ed = pref.edit()
                ed.putString("debo", x).apply()
            } else {
                var x = debo.plus(",").plus(us).plus("-").plus(pred)
                var ed = pref.edit()
                ed.putString("debo", x).apply()
            }
        }


    }
    fun showAlert()
    {
        Toast.makeText(this,"No hay conexión a internet pero fue guardado exitosamente",Toast.LENGTH_LONG).show()
    }

    fun checkCon():Boolean{
        val cm=getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val ac=cm.activeNetworkInfo
        val isC= ac!=null && ac.isConnectedOrConnecting
        return isC
    }
}


