package com.example.prototipo3

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore


class FormCrear : AppCompatActivity() {
    val db = FirebaseFirestore.getInstance()
    private lateinit var nombre:EditText
    private lateinit var correo:EditText
    private lateinit var contr:EditText
    private lateinit var auth: FirebaseAuth
    private lateinit var progessBar:ProgressBar
    private lateinit var dbRef:CollectionReference



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_crear)


        nombre=findViewById(R.id.nom)
        correo=findViewById(R.id.correo)
        contr=findViewById(R.id.contr)
        auth = FirebaseAuth.getInstance()
        progessBar=findViewById(R.id.progressBar)
        dbRef=db.collection("usuarios")




        val cancelar:Button =findViewById(R.id.cancelar)
        val aceptar:Button=findViewById(R.id.aceptar)
        aceptar.setOnClickListener(){
            create()
        }
        cancelar.setOnClickListener (){
            finish()
        }
    }

    private fun create()
    {
        val name:String=nombre.text.toString()
        val correo:String=correo.text.toString()
        val contr:String=contr.text.toString()



        if(!name.equals("")&& !correo.equals("")&& !contr.equals("") )
        {
            if(checkCon())
            {

                auth.createUserWithEmailAndPassword(correo, contr)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            val user: FirebaseUser? = auth.currentUser

                            Toast.makeText(
                                this,
                                "Se creo correctamente el usuario",
                                Toast.LENGTH_LONG
                            ).show()
                            val userp = hashMapOf(
                                "nombre" to name,
                                "email" to correo,
                                "ID" to task.result!!.user!!.uid


                            )
                            dbRef.add(userp)

                            finish()
                        } else {
                            Toast.makeText(this, "Error al crear usuario", Toast.LENGTH_LONG).show()

                        }

                    }
            }
            else
            {
                showAlertInternet()
            }
        }
        else
        {
            Log.i("else","cae en el if")
        }
    }
    fun checkCon():Boolean{
        val cm=getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val ac=cm.activeNetworkInfo
        val isC= ac!=null && ac.isConnectedOrConnecting
        return isC
    }
    fun showAlertInternet()
    {

        Toast.makeText(this,"No hay conexion a internet en este momento",Toast.LENGTH_LONG).show()
    }
}
