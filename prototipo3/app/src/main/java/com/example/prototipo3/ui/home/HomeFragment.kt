package com.example.prototipo3.ui.home

import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.opengl.EGL14
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.prototipo3.*
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.FirebaseFirestore

import org.jetbrains.anko.toast
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.activity_confirmacion.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.AlertDialogBuilder
import java.io.InputStream
import java.io.OutputStream
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private val key ="MY_KEY"
    val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
    lateinit var ped:String
    val db = FirebaseFirestore.getInstance()
    private lateinit var firebaseAnalytics: FirebaseAnalytics




    var dev: ArrayList<String> = ArrayList()
    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        val pedidp:Button = root.findViewById(R.id.pedido)
        val b:EditText =root.findViewById(R.id.editText)
        val c:Spinner = root.findViewById(R.id.unidades)
        val d:Spinner=root.findViewById(R.id.spinProd)

        firebaseAnalytics = FirebaseAnalytics.getInstance(root.context)

        pedidp.setOnClickListener() {
            try
            {

            if (BluetoothAdapter.getDefaultAdapter() == null) {
                showAlert("Por favor verifique la conexión bluetooth", "Error de bluetooth")
            } else if (!BluetoothAdapter.getDefaultAdapter().isEnabled) {
                showAlert("Por favor encienda bluetooth", "Error de bluetooth")
            } else if (((b.text.toString()).toDouble()) <= 0) {
                showAlert("Por favor ingrese una cantidad mayor a 0", "Cantidad erronea")
            } else if (!d.selectedItem.toString().equals("Cerveza") && c.selectedItem.toString().equals(
                    "Litros"
                )
            ) {
                showAlert(
                    "Por favor ingrese un producto que se pueda medir en litros",
                    "Error de pedido"
                )
            } else if (d.selectedItem.toString().equals("Uvas") && !c.selectedItem.toString().equals(
                    "KG"
                )
            ) {
                showAlert("Por favor ingrese una unidad valida para las Uvas", "Error de pedido")
            } else {
                var m: String =
                    bluetoothAdapter?.name.plus("-").plus(b.text.toString()).plus("-")
                        .plus(c.selectedItem.toString()).plus("-").plus(d.selectedItem.toString())


                var i = Intent(activity, GifBanana::class.java)

                ped = m

                save()

                Log.i("Pedido", ped + "")

                val bundle = Bundle()

                bundle.putString("ProductoSel", d.selectedItem.toString())
                bundle.putString("UnidadSel", c.selectedItem.toString())
                bundle.putString("Cantidad", b.text.toString())
                firebaseAnalytics.logEvent("Pedidos", bundle)
                val prod = Bundle()
                prod.putString("NombreProd",d.selectedItem.toString())
                firebaseAnalytics.logEvent("Producto", prod)
                val persona = Bundle()
                persona.putString("usuario",bluetoothAdapter?.name)
                firebaseAnalytics.logEvent("usuario", persona)
                startActivity(i)
                b.text.clear()

            }
        }
            catch(e:Exception)
            {
                showAlert("Por favor ingrese una cantidad valida", "Error de pedido")
            }
        }

        return root
    }

    fun showAlert(men:String,tit:String)
    {

        val builder= AlertDialog.Builder(activity)
        builder.setTitle(tit)
        builder.setMessage(men)
        val dialog=builder.create()
        dialog.show()
    }




    fun save()
    {
        val pref = PreferenceManager.getDefaultSharedPreferences(activity)
        var edit=pref.edit()
        edit.putString("momen",ped).apply()
    }
}


