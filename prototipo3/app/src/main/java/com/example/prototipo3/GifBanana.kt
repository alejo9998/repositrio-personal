package com.example.prototipo3

import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_gif_banana.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.toast
import java.io.InputStream
import java.io.OutputStream
import java.util.*

class GifBanana : AppCompatActivity() {

    lateinit var m_pairDevices:Set<BluetoothDevice>
    lateinit var t:BluetoothClient
    val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gif_banana)

        bluetoothAdapter?.startDiscovery()
        bluetoothAdapter?.cancelDiscovery()
        pairedDevicedList()

  //      val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)

//       registerReceiver(receiver, filter)
       var ped =  (PreferenceManager.getDefaultSharedPreferences(this)).getString("momen","no hay")
        t =BluetoothClient(m_pairDevices, this,ped)
        try {
            t.start()


        } catch (e: Exception) {
            Log.i("error", "error al conectar al disp" )
        }
        cancelar.setOnClickListener {
            Log.i("thread", "parando el thread")
            t.sigui=false
            t.interrupt()
            finish()

        }
    }

    override fun onBackPressed() {
        super.onBackPressed()


    }

    fun fin()
    {

       finish()

    }

    private val receiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val action: String = intent.action.toString()
            Log.i("Esta "," escaneado")
            when(action) {
                BluetoothDevice.ACTION_FOUND -> {
                    // Discovery has found a device. Get the BluetoothDevice
                    // object and its info from the Intent.
                    val device: BluetoothDevice =
                        intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                    val deviceName = device.name
                    Log.i("disp",""+deviceName)
                    val deviceHardwareAddress = device.address // MAC address
                }
            }
        }
    }



    private fun pairedDevicedList(){
        m_pairDevices = bluetoothAdapter!!.bondedDevices
        val list: ArrayList<BluetoothDevice> = ArrayList()


        if(!m_pairDevices.isEmpty()) {
            for (device: BluetoothDevice in m_pairDevices) {
                list.add(device)

                Log.i("device ", "" + device)
            }
        }
        else
        {
            toast("no hay disspositivos cerca")
        }

        return
    }

    fun pantSig()
    {
        t.interrupt()
        startActivity(Intent(this,Confirmacion::class.java))
        finish()
    }






}
