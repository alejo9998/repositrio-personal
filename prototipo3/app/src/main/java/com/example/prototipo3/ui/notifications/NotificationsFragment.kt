package com.example.prototipo3.ui.notifications

import android.app.Activity
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.prototipo3.MainActivity
import com.example.prototipo3.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

class NotificationsFragment : Fragment() {

    private lateinit var notificationsViewModel: NotificationsViewModel
    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()
    lateinit var dbRef: DocumentReference
    lateinit var pref: SharedPreferences
    lateinit var  acti:Activity
    lateinit var mom:CollectionReference
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
            ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_notifications, container, false)

        auth = FirebaseAuth.getInstance()

        acti=(activity as MainActivity).act()


        var pref= PreferenceManager.getDefaultSharedPreferences(root.context)
        var correo: String? = pref.getString("usuario", "No hay")
        var contr: String? = pref.getString("contr", "No hay")
        var id = pref.getString("id", "No hay")

        var el=pref.getString("deben","")
        if(el.equals(""))
        {
            el="Nadie te debe"

        }

            var list: ListView =root.findViewById(R.id.listaDebe)
            var a= ArrayAdapter(root.context,android.R.layout.simple_selectable_list_item,el!!.split(","))
            list.adapter=a





        return root
    }
}