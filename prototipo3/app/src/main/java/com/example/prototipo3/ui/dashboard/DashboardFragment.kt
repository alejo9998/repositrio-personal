package com.example.prototipo3.ui.dashboard

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.prototipo3.MainActivity
import com.example.prototipo3.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlin.math.log

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel
    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()
    lateinit var dbRef: DocumentReference
    lateinit var pref: SharedPreferences
    lateinit var  acti: Activity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)

        dashboardViewModel.text.observe(this, Observer {

        })
        activity!!.window.statusBarColor
        auth = FirebaseAuth.getInstance()

        acti=(activity as MainActivity).act()


        var pref= PreferenceManager.getDefaultSharedPreferences(root.context)
        var list: ListView =root.findViewById(R.id.listDebo)

        var el=pref.getString("debo","")

        Log.i("debo",el)
        if(el.equals(""))
        {
            el="No le debes a nadie"
        }

        var a = ArrayAdapter(
                root.context,
                android.R.layout.simple_selectable_list_item,
                el!!.split(",")
         )


        list.adapter = a
        return root
    }

}