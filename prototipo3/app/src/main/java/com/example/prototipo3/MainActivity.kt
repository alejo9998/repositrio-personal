package com.example.prototipo3

import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.content.DialogInterface

import android.content.Intent

import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import org.jetbrains.anko.toast
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*
import java.io.IOException
import java.util.*


class MainActivity : AppCompatActivity() {


    lateinit var bluetoothAdapter: BluetoothAdapter

    val REQUEST_ENABLE_BLUETOOTH = 1

    companion object {
        val EXTRA_ADDRESS: String = "Devices_address"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        if (logIn()) {
            super.onCreate(savedInstanceState)

            var i = Intent(this, LogIn::class.java)
            startActivity(i)
            finish()
        } else {

            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)
            val navView: BottomNavigationView = findViewById(R.id.nav_view)

            val navController = findNavController(R.id.nav_host_fragment)


            val appBarConfiguration = AppBarConfiguration(
                setOf(
                    R.id.navigation_dashboard, R.id.navigation_home, R.id.navigation_notifications
                )
            )
            setupActionBarWithNavController(navController, appBarConfiguration)
            navView.setupWithNavController(navController)

            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            if (bluetoothAdapter == null) {
                toast("El dispositivo no tiene bluetooth")
                return
            }
            if (!bluetoothAdapter!!.isEnabled) {
                val enBluInte = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enBluInte, REQUEST_ENABLE_BLUETOOTH)
            }




            try {

                lifecycleScope.launch {
                    Log.d("thread","1-" +(Looper.myLooper()== Looper.getMainLooper()))
                    escucharConexiones()
                }
                Log.d("thread","4-" +(Looper.myLooper()== Looper.getMainLooper()))
                /*runOnUiThread {


                        BluetoothServerController(this).start()

                }

*/
            } catch (e: Exception) {

            }



        }
    }


    fun logIn(): Boolean {
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        val gua = pref.getString("usuario", "No hay nada guardado")!!
        return (gua == "No hay nada guardado")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                if (bluetoothAdapter!!.isEnabled) {
                    toast("Bluetooth esta activado")
                } else {
                    toast("Bluetooth esta desactivado")
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                toast("Activación del bluettoth fue cancelada")
            }
        }


    }

    fun showAlert() {

    }

    fun act(): Activity {
        return this
    }

    suspend fun escucharConexiones() {

        val serverSocket: BluetoothServerSocket?


        val btAdapter = BluetoothAdapter.getDefaultAdapter()
        if (btAdapter != null) {
            serverSocket = btAdapter.listenUsingRfcommWithServiceRecord("test", uuid) // 1

        } else {
            serverSocket = null

        }
        withContext(Dispatchers.IO) {
            var socket: BluetoothSocket
            Log.d("thread","2-" +(Looper.myLooper()== Looper.getMainLooper()))

            try {
                socket = serverSocket!!.accept()  // 2
                if (socket != null) {
                    Log.i("server", "Connecting")
                    val inputStream = socket.inputStream
                    val outputStream = socket.outputStream
                    val id:String?=PreferenceManager.getDefaultSharedPreferences(act()).getString("id","No hay id")
                    try {
                        val sc = Scanner(inputStream)
                        val available = inputStream.available()
                        val bytes = ByteArray(available)
                        Log.i("hua", "Reading")

                        lateinit var ped:String
                        var i=0
                        while (sc.hasNext())
                        {

                            Log.i("hua", "Message received")

                            var txt = sc.nextLine()
                            Log.i("hua", "Aqui va el mensaje---->" + txt)
                            if(txt.equals("ya"))
                            {
                                break
                            }
                            if(i==30)
                            {
                                throw Exception()
                            }
                            i=i+1
                        }
                        outputStream.write(("id:"+id!!).toByteArray())
                        Log.i("hua", "Sending" )

                        outputStream.flush()


                        outputStream.write(('\n'+"ya").toByteArray())
                        Log.i("hua", "Sending" )
                        outputStream.flush()
                        outputStream.write(('\n'+"ya").toByteArray())
                        Log.i("hua", "Sending" )
                        outputStream.flush()

                        var txt = sc.nextLine()
                        Log.i("mensaje ya",""+txt)

                        txt = sc.nextLine()
                        Log.i("mensaje pedido",""+txt)

                        ped = sc.nextLine()
                        Log.i("ped",""+ped)

                        var resp="No"
                        withContext(Dispatchers.Main) {
                            resp =alert(ped)

                        }

                        outputStream.write(('\n'+resp).toByteArray())
                        Log.i("hua", "Sending si" )
                        outputStream.flush()
                        outputStream.write(('\n'+resp).toByteArray())
                        Log.i("hua", "Sending si" )
                        dato(ped)
                        outputStream.flush()


                        sc.close()
                    } catch (e: Exception) {
                        Log.e("hua", "Cannot read data", e)
                    }

                    finally {
                        inputStream.close()
                        outputStream.close()
                        socket.close()
                    }
                    //BluetoothServer(act(), socket).run()
                    withContext(Dispatchers.Main)
                    {
                        Log.d("thread","3-" +(Looper.myLooper()== Looper.getMainLooper()))
                        Toast.makeText(act(), "prueba mesage", Toast.LENGTH_LONG).show()

                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()

            }

        }
        escucharConexiones()

    }
    suspend fun alert(men:String):String
    {
        var prestar="No"
        val s=men.split("-")
        val builder= AlertDialog.Builder(this)
        builder.setTitle("Un papayamigo te necesita")
        builder.setPositiveButton("Prestar",DialogInterface.OnClickListener { dialog, which ->
            prestar="si"
        }
        )
        builder.setNegativeButton("No prestar",DialogInterface.OnClickListener(){dialog, which ->
            prestar="no"
        })
        builder.setMessage(s[0]+" quiere pedirte prestado: \n"+s[1]+" "+s[2] +" de "+s[3])
        val dialog=builder.create()
        dialog.show()

        Log.i("valor prestar",prestar)
        return prestar

    }

    fun dato(ped:String)
    {
        var pref= PreferenceManager.getDefaultSharedPreferences(this)
        var debo =  pref.getString("deben","")
        if(debo.equals(""))
        {
            var x=debo.plus(ped)
            var ed=pref.edit()
            ed.putString("deben",x).apply()
        }
        else
        {
            var x=debo.plus(",").plus(ped)
            var ed=pref.edit()
            ed.putString("deben",x).apply()
        }
    }
}









