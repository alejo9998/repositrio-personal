package com.example.prototipo3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_confirmacion.*

class Confirmacion : AppCompatActivity() {




    var nom:String?= "error"

    var counter =10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmacion)
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        nom=pref.getString("nom","error")
        var txt= findViewById(R.id.contador) as TextView

        var txt2=findViewById(R.id.textView4) as TextView
        cancelar.setOnClickListener{

            finish()

        }
        aceptar.setOnClickListener{
            var i = Intent(this, gesto::class.java)

            startActivity(i)
            finish()

        }
        val t = object :Thread(){
            override fun run() {
                while (!isInterrupted)
                {
                    try{
                        Thread.sleep(1000)

                        runOnUiThread{
                            counter-=1


                            if(counter.toInt()==0)
                            {
                                finish()
                            }
                            txt2.text=nom+" te va a prestar"
                            txt.text=counter.toString()
                        }
                    }
                    catch (e:InterruptedException)
                    {
                        e.printStackTrace()
                    }
                }
            }

        }
        t.start()
    }



}
